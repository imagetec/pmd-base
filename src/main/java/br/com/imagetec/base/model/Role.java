package br.com.imagetec.base.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Entity
@Component
public class Role implements GrantedAuthority{
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "IdRoleSeq")
	@SequenceGenerator(name = "IdRoleSeq", sequenceName = "IdRoleSeq", allocationSize = 1, initialValue = 1)
	private long id;
	
	@Column(unique=true)
	private String nome;
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy="roles")
	private List<UsuarioInterno> usuariosInternos = new ArrayList<UsuarioInterno>();
	 
	public Role(){}
	
	public Long getId(){
		return this.id;
	}
	
	public void setId(Long id){ 
		this.id = id; 
	}
	
	public Role(String nome){
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "Role [nome=" + nome + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String getAuthority() {
		return this.nome;
	}

	public List<UsuarioInterno> getUsuariosInternos() {
		return usuariosInternos;
	}
	
	public void addUsuarioInterno(UsuarioInterno usuarioInterno) {

		if (usuarioInterno == null) {
			throw new NullPointerException("Usuario Interno não pode nulo");
		}
		//Objects.requireNonNull(usuarioInterno, "Usuario Interno não pode nulo");
		
		if (!this.usuariosInternos.contains(usuarioInterno)) {
			this.usuariosInternos.add(usuarioInterno);
		}
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
}
