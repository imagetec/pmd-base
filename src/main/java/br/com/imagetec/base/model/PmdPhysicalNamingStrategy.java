package br.com.imagetec.base.model;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;


/**
 * Strategy usado para padrao de nomes de novas tabelas utilizadas no projeto.
 *
 * @author image
 */
public class PmdPhysicalNamingStrategy extends PhysicalNamingStrategyStandardImpl {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Retorna o prefixo das tabelas utilizadas no sistema
     *
     * @return Prefixo das tabelas utilizadas no sistema
     */
    protected String getTablePrefix() {
        return "prot_";
    }
    
    private String transformTableName(String className){
    	 
        className = className
                .replaceAll("(\\w)([A-Z])", "$1_$2")
                .toLowerCase();
        return className;
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
    	
    	String className = name.getText();

        name = Identifier.toIdentifier(getTablePrefix() + transformTableName(name.getText()));
        
        return super.toPhysicalTableName(name, context);
    }
    
    public static String getClassPackage() {
		return PmdPhysicalNamingStrategy.class.getPackage().getName();
	}

}
