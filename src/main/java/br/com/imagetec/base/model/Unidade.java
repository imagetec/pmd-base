package br.com.imagetec.base.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * 
 * @author Kalil Reis de Sisto
 *
 */

@Entity
public class Unidade implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_UNIDADE")
    @SequenceGenerator(name = "SEQ_UNIDADE", sequenceName = "SEQ_UNIDADE", allocationSize = 1)
   
	private Long id;
    
    @Column(length = 100, nullable = false, unique = true)
    private String nome;
    
    @Column(length = 50, nullable = false, unique = false)
    private String area;
    
    @Column(length = 50, nullable = false, unique = true)
    private String sigla;
    
    private boolean ativo = true;
    
    @Column
    private TipoOrigem identificacao;
    
    @ManyToMany
    @JoinTable(name = "UniUsuLid", 
    		   joinColumns = { @JoinColumn(name = "UNIDADE_ID") }, 
    		   inverseJoinColumns = { @JoinColumn(name = "USU_LID_ID") })
    private List<UsuarioInterno> usersLideres;
    
	@ManyToMany
	@JoinTable(name = "UniUsuFun", 
	 		   joinColumns = { @JoinColumn(name = "UNIDADE_ID") }, 
	 		   inverseJoinColumns = { @JoinColumn(name = "USU_FUNC_ID") })
    private List<UsuarioInterno> usersFuncionarios;
    
    @Transient @ManyToOne
    private Unidade unidadeIncorporadora;
    
    @Transient @OneToMany(mappedBy = "unidadeIncorporadora", cascade = CascadeType.ALL)
    private List<Unidade> unidadesIncorporadas;
    
    protected Unidade(){ 
    	this.identificacao = TipoOrigem.AMBOS;
    }
    
	private Unidade(String area, boolean isAtivo, String nome, String sigla,  List<UsuarioInterno> lideres,
			List<UsuarioInterno> funcionarios, TipoOrigem identificacao) {
		
		
		this.nome = nome;
		this.area = area;
		this.sigla = sigla;
		this.usersFuncionarios = funcionarios;
		this.usersLideres = lideres;
		this.identificacao = identificacao;
		
	}

	public static Unidade create(){
		return new Unidade();
	}
	
	
	public static Unidade create(Long id, String area, boolean isAtivo, String nome, String sigla, TipoOrigem identificacao){
		
		List<UsuarioInterno> listaVazia = new ArrayList<UsuarioInterno>();
		
		return new  Unidade(area, isAtivo, nome, sigla, listaVazia, listaVazia, identificacao);
	}
	
	public static Unidade create(String nome, String area, String sigla, TipoOrigem identificacao) {
		
		ArrayList<UsuarioInterno> lideres = new ArrayList<UsuarioInterno>();
		ArrayList<UsuarioInterno> funcionarios = new ArrayList<UsuarioInterno>();
		
		return new  Unidade(area, true, nome, sigla, funcionarios, lideres, identificacao);
	}
	
	public static Unidade create(String nome, String area, String sigla, List<UsuarioInterno> lideres,
			List<UsuarioInterno> funcionarios, TipoOrigem identificacao) {
		
		return new  Unidade(area, true, nome, sigla, funcionarios, lideres, identificacao);
	}
	
	@Override
	public String toString() {
		return "Unidade [id=" + id + ", nome=" + nome + ", area=" + area + ", sigla=" + sigla + ","
				+ " ativo=" + ativo + "]";
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<UsuarioInterno> getUsersLideres() {
		return usersLideres;
	}

	public void setUsersLideres(List<UsuarioInterno> usersLideres) {
		this.usersLideres = usersLideres;
	}

	public List<UsuarioInterno> getUsersFuncionarios() {
		return usersFuncionarios;
	}

	public void setUsersFuncionarios(List<UsuarioInterno> usersFuncionarios) {
		this.usersFuncionarios = usersFuncionarios;
	}

	public Unidade getUnidadeIncorporadora() {
		return unidadeIncorporadora;
	}

	public void setUnidadeIncorporadora(Unidade unidadeIncorporadora) {
		this.unidadeIncorporadora = unidadeIncorporadora;
	}

	public List<Unidade> getUnidadesIncorporadas() {
		return unidadesIncorporadas;
	}

	public void setUnidadesIncorporadas(List<Unidade> unidadesIncorporadas) {
		this.unidadesIncorporadas = unidadesIncorporadas;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public void addLider(UsuarioInterno usuarioInterno){
		
		if (!this.usersLideres.contains(usuarioInterno)) {
			this.usersLideres.add(usuarioInterno);
		}
		
	};
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unidade other = (Unidade) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public void addFuncionario(UsuarioInterno usuarioInterno){
	
		if (!this.usersFuncionarios.contains(usuarioInterno)) {
			this.usersFuncionarios.add(usuarioInterno);
		}
	}

	public TipoOrigem getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(TipoOrigem identificacao) {
		this.identificacao = identificacao;
	};
	
}
