package br.com.imagetec.base.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * 
 * @author Kalil Reis de Sisto
 *
 */
@Component
@Entity
public class UsuarioInterno implements UserDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_USUARIO_INTERNO")
	@SequenceGenerator(name = "SEQ_USUARIO_INTERNO", sequenceName = "SEQ_USUARIO_INTERNO", allocationSize = 1)
	private Long id;

	@Column(length = 150, nullable = false)
	private String nome;

	@Column(length = 20)
	private String celular;

	@Column(length = 100)
	private String email;

	@Column(unique = true)
	private Long cpf;

	@Column(length = 50)
	private String passHashSalted;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastPassChange;

	private Integer passRetries;

	private boolean active = true;

	@Temporal(TemporalType.TIMESTAMP)
	private Date expirationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date emailValidationDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date celularValidationDate;

	@Column(length = 52)
	private String pkUsuarioAgiles;
	
	@Column(length = 100)
	private String idChaveKms;
	
	private String primeiroNome;
	
	private String sobrenome;
	
	/*
	 * Atributos PMD-PE
	 */

	private String prontuario;
	
	private String password;

	private String pic;
	
	/*
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "UsuSubstituicao", 
	   		   joinColumns = { @JoinColumn(name = "USU") }, 
	   		   inverseJoinColumns = { @JoinColumn(name = "SUBS") })
	private List<SubstituicaoUsuario> substituicoes;
	*/
	
	@OneToMany (cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_RESPONSAVEL")
    private List<SubstituicaoUsuario> subsResponsavel;

	
	@OneToMany (cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SUBSTITUIDO")
    private List<SubstituicaoUsuario> subsSubstituido;
	
	
	@OneToMany (cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SUBSTITUTO")
    private List<SubstituicaoUsuario> subsSubstituto;
	
	@ManyToMany
	@JoinTable(name = "UniUsuLid", 
	   		   joinColumns = { @JoinColumn(name = "USU_LID_ID") }, 
	   		   inverseJoinColumns = { @JoinColumn(name = "UNIDADE_ID") })
	private List<Unidade> liderDasUnidades;
	 
	@ManyToMany
	@JoinTable(name = "UniUsuFun", 
	 		   joinColumns = { @JoinColumn(name = "USU_FUNC_ID") }, 
	 		   inverseJoinColumns = { @JoinColumn(name = "UNIDADE_ID") })
	private List<Unidade> funcionarioDasUnidades;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "RoleUsu", 
				joinColumns = { @JoinColumn(name = "`USU_ID`") }, 
				inverseJoinColumns = {
				@JoinColumn(name = "`ROLE_ID`") })
	private List<Role> roles;

	static BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	public UsuarioInterno() {
		roles = new ArrayList<Role>();
	}

	private UsuarioInterno(Long id, String nome, String email, String password, String pic, Long cpf,
			List<Role> roles) {
		this.id = id;
		this.nome = nome;
		this.email = email;
		this.password = password;
		this.pic = pic;
		this.cpf = cpf;
		this.roles = roles;
	}

	public static UsuarioInterno create() {
		return new UsuarioInterno();
	}

	public static UsuarioInterno create(String nome, String email, String password, String pic, Long cpf) {

		return new UsuarioInterno(null, nome, email, passwordEncoder.encode(password), pic, cpf, new ArrayList<Role>());

	}

	@Override
	public String toString() {
		return "UsuarioInterno [id=" + id + ", nome=" + nome + ", celular=" + celular + ", email=" + email +
				", cpf="	+ cpf + ", passHashSalted=" + passHashSalted + ", lastLogin=" + lastLogin +
				", lastPassChange="	+ lastPassChange + ", passRetries=" + passRetries + ", active=" + active +
				", expirationDate="	+ expirationDate + ", emailValidationDate=" + emailValidationDate +
				", celularValidationDate=" + celularValidationDate + ", pkUsuarioAgiles=" + pkUsuarioAgiles +
				", password=" + password + ", pic=" + pic + ", idChaveKms="+ idChaveKms +
				", liderDasUnidades=" + liderDasUnidades + ", funcionarioDasUnidades=" + funcionarioDasUnidades +
				", roles=" + roles + "]";
	}


	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public Long getCpf() {
		return cpf;
	}
	public String getCpfAsString() {
		return String.format("%011d", getCpf());
	}
	
	public String getCpfFormatado() {
		Pattern pattern = Pattern.compile("(\\d{3})(\\d{3})(\\d{3})(\\d{2})");
		Matcher matcher = pattern.matcher(getCpfAsString());
		return matcher.replaceAll("$1.$2.$3-$4");
	}
	
	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public void limparDadosSigilosos() {
		this.password = "";
		this.cpf = 0L;
	}

	public List<Role> getRoles() {
		
		if(roles !=null)
			return new ArrayList<Role>(roles);
		
		return new ArrayList<Role>();
	}

	public void addRole(Role role) {
		Objects.requireNonNull(role, "Role não pode ser nula.");
		
		if (!this.roles.contains(role)) {
			this.roles.add(role);
		}
		
	}

	public boolean removeRole(Role role) {
		Objects.requireNonNull(role, "Role não pode ser nula.");
		return this.roles.remove(role);
	}
	
	public String getProntuario() {
		return prontuario;
	}

	public void setProntuario(String prontuario) {
		this.prontuario = prontuario;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCelular() {
		return celular;
	}

	public String getPassHashSalted() {
		return passHashSalted;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public Date getLastPassChange() {
		return lastPassChange;
	}

	public Integer getPassRetries() {
		return passRetries;
	}

	public boolean isActive() {
		return active;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public Date getEmailValidationDate() {
		return emailValidationDate;
	}

	public Date getCelularValidationDate() {
		return celularValidationDate;
	}
	
	public void setCelular(String celular) {
		this.celular = celular;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public String getIdChaveKms() {
		return idChaveKms;
	}
	
	public void setIdChaveKms(String idChaveKms) {
		this.idChaveKms = idChaveKms;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsuarioInterno other = (UsuarioInterno) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		return true;
	}

	@Override // @JsonIgnore
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.roles;
	}

	@Override
	public String getUsername() {
		return this.email;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO : Desenvolver regra para expiração de conta de usuário interno
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO : Desenvolver regra para bloqueio de conta de usuário interno
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO : Desenvolver regra para expiração de credenciais
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public List<Unidade> getLiderDasUnidades() {
		return liderDasUnidades;
	}

	public void setLiderDasUnidades(List<Unidade> liderDasUnidades) {
		this.liderDasUnidades = liderDasUnidades;
	}

	public List<Unidade> getFuncionarioDasUnidades() {
		return funcionarioDasUnidades;
	}

	public void setFuncionarioDasUnidades(List<Unidade> funcionarioDasUnidades) {
		this.funcionarioDasUnidades = funcionarioDasUnidades;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPkUsuarioAgiles() {
		return pkUsuarioAgiles;
	}

	public void setPkUsuarioAgiles(String pkUsuarioAgiles) {
		this.pkUsuarioAgiles = pkUsuarioAgiles;
	}
	
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public String getPrimeiroNome() { 
		int index = 0;
		try{ 
			index = nome.indexOf(" "); 
			primeiroNome = nome.substring(0, index); 
	    }catch(Exception e){
	    	if(index < 0){
	    		return nome;
	    	}else{
	    		return primeiroNome;
	    	}
	    } 
		return primeiroNome; 
	}
	 
	public String getSobrenome() { 
		try{ 
			int index = nome.indexOf(" "); 
			int tamanho = nome.length(); 
			sobrenome = nome.substring(index+1, tamanho); 
		}catch(Exception e){ 
			e.printStackTrace(); 
		return ""; 
		}
	return sobrenome; 
	}

	public List<SubstituicaoUsuario> getSubsResponsavel() {
		return new ArrayList<SubstituicaoUsuario>(subsResponsavel);
	}

	public List<SubstituicaoUsuario> getSubsSubstituido() {
		return new ArrayList<SubstituicaoUsuario>(subsSubstituido);
	}

	public List<SubstituicaoUsuario> getSubsSubstituto() {
		return new ArrayList<SubstituicaoUsuario>(subsSubstituto);
	}
	
	public List<SubstituicaoUsuario> getQuemEuSubstituo() {
		return new ArrayList<SubstituicaoUsuario>(subsSubstituto);
	}
	
	/*
	public void removerSubstituicao(SubstituicaoUsuario su) {
		
		if (su.getSituacao().equals(SituacaoSubstituicao.AGUARDANDO_INICIO))
			this.substituicoes.remove(su);
		else
			throw new RuntimeException("Não é possível remover uma substituição em " + su.getSituacao());
	}

	public void addSubstituicao(SubstituicaoUsuario substituicao) {
		
		if (!this.substituicoes.contains(substituicao)) {
			
			if(!this.equals(substituicao.getSubstituido()) && 
			   !this.equals(substituicao.getSubstituto()))
				throw new IllegalArgumentException("O usuário deve ser o substituto ou o substituído.");
			
			this.substituicoes.add(substituicao);
		}
	}
	*/
}
