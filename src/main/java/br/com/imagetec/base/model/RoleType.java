package br.com.imagetec.base.model;

public enum RoleType {

	ROLE_LIDER("Coordenador"),
	ROLE_ASSINANTE("Assinante"),//Quem deve, obrigatoriamente, assinar documentos. 
	ROLE_USER("Usuário"), 
	ROLE_FUNCIONARIO("Analista"), 
	ROLE_MUNICIPE("Munícipe"),
	ROLE_ADMIN("Administrador"),
	ROLE_GESTOR("Gestor"),
	ROLE_ZELADORIA("Zeladoria"),
	ROLE_AUDITOR("Auditor");
	
	private String label;
	
	RoleType(String label){
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getName() {
		return this.name();
	}
	
}
